class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :job_id
      t.integer :task_id
      t.date :due_date
      t.string :name
      t.string :location
      t.boolean :complete

      t.timestamps
    end
  end
end
