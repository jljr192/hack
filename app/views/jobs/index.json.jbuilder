json.array!(@jobs) do |job|
  json.extract! job, :id, :job_id, :task_id, :due_date, :name, :location, :complete
  json.url job_url(job, format: :json)
end
